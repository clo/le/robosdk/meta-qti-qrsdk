#License applicable to the recipe file only,  not to the packages installed by this recipe.
LICENSE = "BSD-3-Clause-Clear"

SRC_URI = "file://config_oss.json"
SRC_URI =+ "file://script_oss"

DEPENDS += "${@bb.utils.contains("DISTRO_FEATURES", "qim_sdk", "qti-qim", "", d)}"
DEPENDS += "${@bb.utils.contains("DISTRO_FEATURES", "qirf_sdk", "qti-robotics", "", d)}"

SDKPATH = "${TOPDIR}/function_sdk/"
SDK_PN = "qirp_sdk"
PACKAGES = "${PN}"
ALLOW_EMPTY:${PN} = "1"
FILES:${PN} = "/${SDK_PN}/"

python () {
    sdk_name = ['qim_sdk', 'qirf_sdk', 'snpe_sdk']
    for var in sdk_name:
        if os.path.exists(os.path.join(d.getVar('SDKPATH'), var)):
            bb.note("Setting %s ..." %var)
            d.appendVar('DISTRO_FEATURES', " " + var)
}


python do_install () {
    import os
    import json
    from pathlib import Path
    import glob
    import subprocess

    # Update FILES:pkg. (Not used.)
    # input:
    #     lists: a pick-up manifest file
    #     pkg  : package you need to configure
    def update_files(lists, pkg, d):
        if not os.path.isfile(lists):
            print(f"Error: file '{lists}' does not exist")
        else:
            with open(lists, 'r') as file:
                f = ' '.join([line.strip() for line in file])
            d.setVar('FILES:' + pkg, f)

    # Translate function, when you depend on gstreamer recipes,
    # you can use this function to find gst recipes' outputs.
    # input:
    #     sysroot_path: the path of recipe-sysroot
    #     old_path: orginal pick-up path
    # output:
    #     new_path: return a path, to specify the real pick-up path
    def translate_path(sysroot_path, old_path):
        package_name = old_path[:old_path.find("/")]
        new_path = sysroot_path + "/" + old_path
        if "gstreamer1.0" in package_name:
            if os.path.exists((componentdir + "/" + armarch + "/" + package_name)):
                new_path = componentdir + "/" + armarch + "/" + old_path
            elif os.path.exists((componentdir + "/%s/" + package_name) %machine_arch):
                new_path = componentdir + ("/%s/" %machine_arch) + old_path
            else:
                bb.warn("%s can not find, please check the location." %package_name)
        return new_path

    # Pick up files with configure file from recipe-sysroot.
    # input:
    #     config_path: the path of configure file
    #     function_sdk_path: the path of recipe-sysroot
    #     product_sdk_path: the path of pick-up outputs
    #     pickup_record: a file to record the pick-up files
    def pickup_files(config_path, function_sdk_path, product_sdk_path, pickup_record):
        path = Path(config_path)
        bb.note("config file: %s" %path)
        if not path.exists():
            bb.fatal("config file: %s not exist" %path)
            return
        config = json.load(path.open(mode='r'))
        bb.note("function sdk info is below: %s" %config['function_sdks'])

        for sdk in config['function_sdks']:
            bb.note("function sdk name is below: %s" %sdk['name'])
            bb.note("function sdk pickup_files is below: %s" %sdk['pickup_files'])
            for pick in sdk['pickup_files']:
                for src in pick['from']:
                    to_path = product_sdk_path + "/" + pick['to']
                    from_path = translate_path(function_sdk_path, src)
                    bb.note("to_path: %s" %to_path)
                    bb.note("from_path: %s" %from_path)
                    bb.note("src: %s" %src[src.rfind("/")+1:])
                    if "/" == pick['to']:
                        find_cmd = "cd %s && find ./ -type f | sed \"s/\.\//%s/g\" >> %s && cd -" %(from_path[:from_path.rfind("/")],"\/",product_sdk_path + pickup_record)
                    else:
                        find_cmd = "cd %s && find ./ -type f | sed \"s/\.\//%s/g\" >> %s && cd -" %(from_path[:from_path.rfind("/")],"\/" + pick['to'].replace('/','\/'),product_sdk_path + pickup_record)
                    subprocess.call(find_cmd,shell=True)
                    for f in glob.glob(from_path):
                        bb.note("f = %s" %f)
                        if not os.path.exists(f):
                            bb.fatal("%s is not exsit, please check your config file" %f)
                        if not (os.path.exists(to_path[:to_path.rfind("/")])):
                            bb.note("%s is not exsit, have create it firstly" %to_path[:to_path.rfind("/")])
                            os.makedirs(to_path[:to_path.rfind("/")])
                        bb.note("pick from <%s> to <%s>" %(f,to_path))
                        copy_cmd = "cp -r %s %s" %(f,to_path)
                        subprocess.call(copy_cmd,shell=True)

    machine_arch = d.getVar('MACHINE_ARCH')
    sysrootdir = d.getVar('RECIPE_SYSROOT')
    componentdir = d.getVar('COMPONENTS_DIR')
    armarch = d.getVar('PACKAGE_ARCH')
    sdkname = d.getVar('SDK_PN')
    outputdir = d.getVar('D') + "/" + sdkname
    configfile_oss = d.getVar('WORKDIR') + "/config_oss.json"
    fileslist = "/pickup_files_list_oss"
    pickup_files(configfile_oss, sysrootdir, outputdir, fileslist)

    # move files_list to data dir
    mv_cmd = "mv %s %s" %(outputdir + fileslist,outputdir + "/data/")
    subprocess.call(mv_cmd,shell=True)
}

SSTATETASKS += "do_generate_qirp_sdk "
SSTATE_OUT_DIR = "${DEPLOY_DIR}/artifacts/"
SSTATE_IN_DIR = "${TOPDIR}/${SDK_PN}"
TMP_SSTATE_IN_DIR = "${TOPDIR}/${SDK_PN}_tmp"

do_generate_qirp_sdk[sstate-inputdirs] = "${SSTATE_IN_DIR}"
do_generate_qirp_sdk[sstate-outputdirs] = "${SSTATE_OUT_DIR}"
do_generate_qirp_sdk[dirs] = "${SSTATE_IN_DIR} ${SSTATE_OUT_DIR} ${TMP_SSTATE_IN_DIR}"
do_generate_qirp_sdk[cleandirs] = "${SSTATE_IN_DIR} ${SSTATE_OUT_DIR} ${TMP_SSTATE_IN_DIR}"
do_generate_qirp_sdk[stamp-extra-info] = "${MACHINE_ARCH}"

# Add a task to generate product sdk
do_generate_qirp_sdk () {
    # generate qirp package
    if [ ! -d ${TMP_SSTATE_IN_DIR}/${SDK_PN} ]; then
        mkdir -p ${TMP_SSTATE_IN_DIR}/${SDK_PN}/
    fi
    cp -r ${WORKDIR}/script_oss/* ${TMP_SSTATE_IN_DIR}/${SDK_PN}/
    cp ${DEPLOY_DIR}/${IMAGE_PKGTYPE}/${PACKAGE_ARCH}/${PN}_*.${IMAGE_PKGTYPE} ${TMP_SSTATE_IN_DIR}/${SDK_PN}/
    cd ${TMP_SSTATE_IN_DIR}
    tar -zcf ${SSTATE_IN_DIR}/${SDK_PN}.tar.gz ./${SDK_PN}/*
    # remove temp dir
    # rm -rf ${TMP_SSTATE_IN_DIR}
}

python do_generate_qirp_sdk_setscene() {
    sstate_setscene(d)
}

addtask do_generate_qirp_sdk_setscene
addtask do_generate_qirp_sdk after do_package_write_ipk before do_rm_work

# add a task to remove multilib in image, will not impact tarbell
remove_multilib () {
    rm -rf ${D}/sysroot/usr/lib/libgstqtimlbase.so
    rm -rf ${D}/sysroot/usr/lib/libgstqtivideobase.so
}

# add a prefunction to reorganize directory structure
do_reorganize_pkg_dir () {
    if [ -d ${PKGDEST}/${PN}/${SDK_PN} ]; then
        mv ${PKGDEST}/${PN}/${SDK_PN}/* ${PKGDEST}/${PN}/
        rm -rf ${PKGDEST}/${PN}/${SDK_PN}
    fi
}

# We only need the packaging tasks - disable the rest
do_patch[noexec] = "1"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_populate_sysroot[noexec] = "1"
do_populate_lic[noexec] = "1"
do_package_qa[noexec] = "1"
do_package[prefuncs] += " remove_multilib"
do_package_write_ipk[prefuncs] += "do_reorganize_pkg_dir"
INSANE_SKIP:${PN} += "already-stripped"
