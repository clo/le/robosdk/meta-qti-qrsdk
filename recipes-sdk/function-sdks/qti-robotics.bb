inherit qrsdk_base_ipk

#License applicable to the recipe file only,  not to the packages installed by this recipe.
LICENSE = "BSD-3-Clause-Clear"

# the information of function sdk package(s)
FILESPATH =+ "${TOPDIR}/function_sdk/qirf_sdk/:"

SRC_URI = "file://opkg.conf"
SRC_URI =+ "file://robotics_function_sdk_oss.tar.gz"
