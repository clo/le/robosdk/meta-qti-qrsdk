# License applies to this recipe code, not to the packages installed by this recipe
inherit qrsdk_base_ipk

LICENSE = "BSD-3-clause-clear"

# the information of function sdk package(s)
FILESPATH =+ "${TOPDIR}/function_sdk/qim_sdk/:"

SRC_URI = "file://opkg.conf"
SRC_URI =+ "file://packages.zip"
