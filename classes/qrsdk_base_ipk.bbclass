#
# QRSDK
#

PATH:prepend = "/usr/bin:"

DEPENDS = "opkg-native"

fakeroot do_install() {
    mkdir -p ${D}/${PN}
    for ipk in `find ${WORKDIR}/ -name "*.ipk" ! -name "*dbg_*" ! -name "*doc_*"`
    do
        echo $ipk
        opkg install $ipk --volatile-cache -f ${WORKDIR}/opkg.conf -o ${D}/${PN}/ --add-arch qrb5165_rb5 --add-arch armv8a --add-arch aarch64  --add-arch all  --force-depends
    done

    rm -rf ${FILES_SKIP}
}

SYSROOT_DIRS = "/"
SYSROOT_DIRS_IGNORE = "/${PN}/${PN}"
FILES:${PN} = "/"

do_unpack[cleandirs] = ""
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_prepare_recipe_sysroot[noexec] = "1"
do_populate_lic[noexec] = "1"
do_package_qa[noexec] = "1"

INSANE_SKIP:${PN} += "already-stripped"
INHIBIT_SYSROOT_STRIP = "1"
EXCLUDE_FROM_SHLIBS = "1"
