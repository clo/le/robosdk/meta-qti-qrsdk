#
# QRSDK
#

PATH:prepend = "/usr/bin:"

# ###################################################
# Below code will be used to cp function-sdk to image
# ###################################################
do_fetch_extra () {
    if [ -f ${SDKSPATH} ];then
        install -d ${S}
        cp -r ${SDKSPATH} ${B}/
    else
        bbfatal "Fail to get the ${SDKSPATH} SDK"
    fi

}

addtask do_fetch_extra before do_fetch

python do_unpack () {
    import os
    from pathlib import Path
    import subprocess

    def extract_sdk(dir):
        if not os.path.exists(dir):
            bb.fatal('{} not exist'.format(dir))
            return
        for entry in os.listdir(dir):
            path = os.path.join(dir, entry)
            if os.path.isdir(path):
                extract_sdk(path)
            else:
                extract_package(path)

    def extract_package(file):
        extract_funcs = [
            ('.tar.gz', extract_targz),
            ('.tgz', extract_targz),
            ('.tar.xz', extract_tarxz),
            ('.txz', extract_txz),
            ('.zip', extract_zip),
            ('.tar', extract_tar),
            ('.deb', extract_deb),
            ('.7z', extract_7z),
        ]
        for func in extract_funcs:
            if (file.endswith(func[0])):
                extract_file(file, func)

    def extract_file(file, func):
        out_path = file[:-len(func[0])]
        bb.note('extract {} to {}'.format(file, out_path))
        if not Path(out_path).exists():
            Path(out_path).mkdir()
        func[1](file, out_path)
        extract_sdk(out_path)

    def extract_targz(file, out_path):
        mkdir_cmd = "mkdir -p %s" %out_path
        subprocess.call(mkdir_cmd,shell=True)
        # tar -zxf file -C out_path
        extract_cmd = "tar -zxf %s -C %s" %(file,out_path)
        subprocess.call(extract_cmd,shell=True)
        pass

    def extract_tarxz(file, out_path):
        mkdir_cmd = "mkdir -p %s" %out_path
        subprocess.call(mkdir_cmd,shell=True)
        # xz -d file && tar -xf file -C out_path
        extract_cmd = "xz -d %s" %file
        subprocess.call(extract_cmd,shell=True)
        extract_cmd = "tar -xf %s -C %s" %(file,out_path)
        subprocess.call(extract_cmd,shell=True)
        pass

    def extract_txz(file, out_path):
        mkdir_cmd = "mkdir -p %s" %out_path
        subprocess.call(mkdir_cmd,shell=True)
        # tar -xJf file -C out_path
        extract_cmd = "tar -xJf %s -C %s" %(file,out_path)
        subprocess.call(extract_cmd,shell=True)
        pass

    def extract_zip(file, out_path):
        # unzip -x file -d file out_path
        extract_cmd = "unzip -x %s -d %s" %(file,out_path)
        subprocess.call(extract_cmd,shell=True)
        pass

    def extract_tar(file, out_path):
        mkdir_cmd = "mkdir -p %s" %out_path
        subprocess.call(mkdir_cmd,shell=True)
        # tar -xf file -C out_path
        extract_cmd = "tar -xf %s -C %s" %(file,out_path)
        subprocess.call(extract_cmd,shell=True)
        pass

    def extract_deb(file, out_path):
        # dpkg -X file out_path
        extract_cmd = "dpkg -X %s %s" %(file,out_path)
        subprocess.call(extract_cmd,shell=True)
        extract_cmd = "dpkg -e %s %s" %(file,out_path)
        subprocess.call(extract_cmd,shell=True)
        pass

    def extract_7z(file, out_path):
        # TODO
        # 7z x file -r -o out_path
        pass

    extract_sdk(d.getVar('S'))
}

python do_install() {
    import os
    import subprocess

    def locate_extract_dir(dir):
        if(len(os.listdir(dir)) == 1 ):
            bb.note(dir + "/" + os.listdir(dir)[0])
            d.setVar("COPYDIR", dir + "/" + os.listdir(dir)[0])
            d.setVar("DIRNAME", os.listdir(dir)[0])
        else:
            for entry in os.listdir(dir):
                path = os.path.join(dir, entry)
                if os.path.isdir(path):
                    locate_extract_dir(path)

    locate_extract_dir(d.getVar('S'))
    copy_cmd = "cp -r %s %s" %(d.getVar("COPYDIR"),d.getVar("D") + "/" + d.getVar("DIRNAME"))
    subprocess.call(copy_cmd,shell=True)

    # remove N/A files
    rm_cmd = "rm -rf %s" %d.getVar("FILES:SKIP")
    subprocess.call(rm_cmd,shell=True)
}

SYSROOT_DIRS = "/${DIRNAME}/"
SYSROOT_DIRS_IGNORE = ""
FILES:${PN} = "/"

do_fetch_extra[cleandirs] = "${S}"
do_unpack[cleandirs] = ""
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_prepare_recipe_sysroot[noexec] = "1"
do_populate_lic[noexec] = "1"
do_package_qa[noexec] = "1"

INSANE_SKIP:${PN} += "already-stripped"
INHIBIT_SYSROOT_STRIP = "1"
EXCLUDE_FROM_SHLIBS = "1"