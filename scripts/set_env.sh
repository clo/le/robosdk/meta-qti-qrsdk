# Copyright (c) 2020, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above
#      copyright notice, this list of conditions and the following
#      disclaimer in the documentation and/or other materials provided
#      with the distribution.
#    * Neither the name of The Linux Foundation nor the names of its
#      contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Changes from Qualcomm Innovation Center are provided under the following license:

# Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
# SPDX-License-Identifier: BSD-3-Clause-Clear

## Script to do pre-configurations specific to current layer
## before calling oe-init-build-env

# Functions SDK dist: [functions SDK file name]=DISTRO_FEATURES
declare -A FUNC_SDK_dist
FUNC_SDK_dist=([ros2_foxy_sdk]=qti-ros-sdk \
               [robotics_function_sdk]=qti-robotics-sdk \
               [snapdragon_npe_sdk]=qti-snpe-sdk \
               [qim_sdk]=qti-qim-sdk \
               [hexgon_sdk]=qti-hexgon-sdk \
               [aic_platform_sdk]=qti-aic-sdk)

LAYERLIST=(poky/meta-qti-qrsdk/
           poky/meta-qti-qrsdk-prop/)

#$BUILDDIR is exported by ESDK enviroment 
BBLAYER_CONF=$BUILDDIR/conf/bblayers.conf
AUTO_CONF=$BUILDDIR/conf/auto.conf
LOCAL_CONF=$BUILDDIR/conf/local.conf

# Configure DISTRO_FEATURES and QRSDK
build_config_func() {
    # if build in ESDK code base
    if [[ $OE_SKIP_SDK_CHECK =~ "1" ]];then
        for layer in ${LAYERLIST[@]}
        do
            if [[ ! "$(cat $BBLAYER_CONF)" =~ $layer ]]; then
                sed -i "/workspace /a\    \${SDKBASEMETAPATH}/layers/${layer} \\\\" ${BBLAYER_CONF}
            fi
        done

        # Get functions SDK info
        for sdk in ${!FUNC_SDK_dist[*]}
        do
            if [[ ! -z `find ${BUILDDIR}/function_sdk -name "*$sdk*"` ]] && [[ ! "$(cat $LOCAL_CONF)" =~ ${FUNC_SDK_dist[${sdk}]} ]];then
                sed -i "/TMPDIR =/i\DISTRO_FEATURES += \"${FUNC_SDK_dist[${sdk}]}\"" ${LOCAL_CONF}
            fi
        done
    # if build in SP code base
    else
        for sdk in ${!FUNC_SDK_dist[*]}
        do
            if [[ ! -z `find ${BUILDDIR}/function_sdk -name "*$sdk*"` ]] && [[ ! "$(cat $AUTO_CONF)" =~ ${FUNC_SDK_dist[${sdk}]} ]];then
                sed -i "/SDK_VERSION =/i\DISTRO_FEATURES += \"${FUNC_SDK_dist[${sdk}]}\"" ${AUTO_CONF}
            fi
        done
    fi
}

# Login function
qpm_login_func () {
    user_username=$(whiptail --title "Username Box" --inputbox "Enter your username and choose Ok to continue." 10 60 3>&1 1>&2 2>&3)
    if [ $? = 1 ];then
        echo "You chose Cancel."
        return
    fi
    user_password=$(whiptail --title "Password Box" --passwordbox "Enter your password and choose Ok to continue." 10 60 3>&1 1>&2 2>&3)
    if [ $? = 1 ];then
        echo "You chose Cancel."
        return
    fi

    echo "Logining......"
    startTime=`date +%Y%m%d-%H:%M:%S`
    startTime_s=`date +%s`

    for i in {1..20}
    do
        login_log=$(qpm-cli --login "$user_username" "$user_password")
        login_status=$(echo $login_log | grep -q "Product catalog refreshed successfully" && echo "TRUE" || echo "FALSE")

        if [ "$login_status" == "TRUE" ];then
            echo "QPM login is successful"
            echo ""
            break
        fi
    done
    
    endTime=`date +%Y%m%d-%H:%M:%S`
    endTime_s=`date +%s`

    sumTime=$[ $endTime_s - $startTime_s ]

    echo "$startTime ---> $endTime" "Total:$sumTime seconds"
}

# Login status check function
qpm_login_status_check_func() {
    check_login_log=$(qpm-cli --check-login)
    check_status=$(echo $check_login_log | grep -q "QPM is not logged in" && echo "FALSE" || echo "TRUE")
    if [ "$check_status" == "TRUE" ];then
        txt_info=$(echo $check_login_log | sed -e 's/\[Info\] : //g')
        if (whiptail --title "Login Check Box" --yesno "$txt_info, do you need to change the current account?" 10 60); then
            qpm_login_func
        fi
    else
        qpm_login_func
    fi
}

# Auth the functions sdk license function
qpm_auth_func() {
    for ITEM in $SDKLIST;do
        echo "checking $ITEM's license..."
        auth_log=$(qpm-cli --license-activate $ITEM)
        auth_status=$(echo $auth_log | grep -q "License activation is successful" && echo "TRUE" || echo "FALSE")
        if [ "$auth_status" == "FALSE" ];then
            echo "[ERROR] Your account is fail to activate new license using <$ITEM>. Please check your license."
            echo ""
            return 1
        else
            echo "$ITEM's license activate successfully"
            echo ""
        fi
    done
}

# Install the latest functions sdk function
qpm_install_func() {
    for ITEM in $SDKLIST; do
        echo ""
        echo "installing $ITEM ..."
        if [ ! -d ${BUILDDIR}/function_sdk ];then
            mkdir -p ${BUILDDIR}/function_sdk
        fi
        latestver=`qpm-cli --info $ITEM | grep "Available version" | awk -F ': ' '{print $2}'`
        echo y | qpm-cli --install $ITEM --path ${BUILDDIR}/function_sdk/ --version $latestver
        echo ""
        if [ $? = 1 ];then
            echo "[ERROR]  Fail to install <$ITEM>"
            return 1
        fi
    done
}

# Build in AU code base
# qpm_login_status_check_func
# qpm_auth_func
# qpm_install_func

build_config_func

